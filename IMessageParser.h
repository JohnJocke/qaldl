/* Copyright (c) 2014, JohnJocke
 * BSD license, check file LICENSE
 */

#ifndef IMESSAGEPARSER_H
#define IMESSAGEPARSER_H

#include <QList>
#include "SensorItem.h"

class IMessageParser
{
public:
    virtual ~IMessageParser(){}

    virtual QList<SensorItem> getSensorList() = 0;
    virtual QByteArray getRequestMessage() = 0;
    virtual int getAnswerLength() = 0;
    virtual QList<double> parseMessage(const QByteArray& message) = 0;

};

#endif // IMESSAGEPARSER_H
