/* Copyright (c) 2014, JohnJocke
 * BSD license, check file LICENSE
 */

#include "CISFI2SParser.h"
#include <QString>

#include <QDebug>

static const int REQ_BYTE_COUNT = 5;
static const char REQ_BYTES[REQ_BYTE_COUNT] = { 0xF4, 0x57, 0x01, 0x00, 0xB4 };

CISFI2SParser::CISFI2SParser()
{
}

CISFI2SParser::~CISFI2SParser()
{
}

QList<SensorItem> CISFI2SParser::getSensorList()
{
    QList<SensorItem> list;

    //list.append(SensorItem("PROM ID", ""));
    list.append(SensorItem("Coolant Value From Slave", "°C"));
    list.append(SensorItem("Startup Coolant Temperature", "°C"));
    list.append(SensorItem("Coolant Temperature", "°C"));
    list.append(SensorItem("Throttle Position sensor voltage", "V"));
    list.append(SensorItem("Throttle Position", "%"));
    list.append(SensorItem("Currently Desired Idle Speed", "RPM"));
    list.append(SensorItem("Engine Speed", "RPM"));
    list.append(SensorItem("Vehicle Speed", "KPH"));
    list.append(SensorItem("O2  Sensor voltage", "mV"));
    list.append(SensorItem("SA Adjust Potentiometer", "V"));
    list.append(SensorItem("Base pulse closed loop correction", "%"));
    //list.append(SensorItem("Base pulse closed loop correction value", ""));
    list.append(SensorItem("Block learn multiplier correction", "%"));
    //list.append(SensorItem("Block learn multiplier cell value", ""));
    list.append(SensorItem("Block learn multiplier cell number in use", ""));
    list.append(SensorItem("IAC present motor position", "steps"));
    list.append(SensorItem("Filtered load variable", ""));
    list.append(SensorItem("Purge duty cycle value", "%"));
    list.append(SensorItem("IAC base motor position warm with no A/C", "steps"));
    list.append(SensorItem("IAC base motor position warm with A/C", "steps"));
    list.append(SensorItem("Battery voltage", "V"));
    list.append(SensorItem("Spark advance relative to TDC", "°"));
    list.append(SensorItem("Fuel pulse delivered after correction", "ms"));
    list.append(SensorItem("Total air/fuel ratio", ""));
    list.append(SensorItem("Engine running time", "s"));
    list.append(SensorItem("Air flow rate from meter", "g/s"));
    list.append(SensorItem("Air flow", "g/s"));
    list.append(SensorItem("CO adjust potentiometer", "V"));
    //list.append(SensorItem("SA Adjust potentiometer", "V"));

    return list;
}

QByteArray CISFI2SParser::getRequestMessage()
{
    return QByteArray(REQ_BYTES, REQ_BYTE_COUNT);
}

int CISFI2SParser::getAnswerLength()
{
    //the answer includes the echo of the request message
    return 72;
}

double CISFI2SParser::parseByte(const QByteArray &message, int idx, double offset, double mult)
{
    double result = static_cast<unsigned char>(message.at(idx));
    result *= mult;
    result += offset;
    return result;
}

double CISFI2SParser::parseRPM(const QByteArray &message, int idx)
{
    unsigned int divider = message.at(idx) & 0xFF;
    divider = divider << 8;
    divider |= message.at(idx + 1) & 0xFF;
    double result = 0x3C0000 / static_cast<double>(divider);
    return result;
}

double CISFI2SParser::parseWord(const QByteArray &message, int idx, double offset, double mult)
{
    unsigned int val = message.at(idx) & 0xFF;
    val = val << 8;
    val |= message.at(idx + 1) & 0xFF;
    double result = val * mult;
    result += offset;
    return result;
}

double CISFI2SParser::parseAirFlow(const QByteArray &message, int idx)
{
    double result = static_cast<unsigned char>(message.at(idx));
    result += (static_cast<unsigned char>(message.at(idx + 1)) * 0.38823 ) / 100;
    return result;
}

double CISFI2SParser::parseAFR(const QByteArray &message, int idx)
{
    unsigned int divider = message.at(idx) & 0xFF;
    divider = divider << 8;
    divider |= message.at(idx + 1) & 0xFF;
    double result = 1638.4 / static_cast<double>(divider);
    return result;
}

QList<double> CISFI2SParser::parseMessage(const QByteArray &message)
{
    //qDebug() << message.toHex();

    QList<double> list;

    //list.append(parsePROMID());
    list.append(parseByte(message, 24, -40, 0.75));
    list.append(parseByte(message, 25, -40, 0.75));
    list.append(parseByte(message, 26, -40, 0.75));

    list.append(parseByte(message, 27, 0, 0.019608)); //throttle pos voltage
    list.append(parseByte(message, 28, 0, 0.392157)); //throttle pos %

    list.append(parseByte(message, 29, 0, 12.5)); //desired idle rpm
    list.append(parseRPM(message, 30)); //engine rpm

    list.append(parseByte(message, 32, 0, 1)); //speed kmh
    list.append(parseByte(message, 33, 0, 4.45)); //lambda mV
    list.append(parseByte(message, 34, 0, 0.02)); //SA potentiometer

    list.append(parseByte(message, 35, 50, 0.390625)); //base pulse corr

    list.append(parseByte(message, 36, 0, 1)); //block multiplier
    //list.append(parseBlockMultiplierCellVal());
    list.append(parseByte(message, 37, 0, 1)); //block multiplier cell number

    list.append(parseByte(message, 38, 0, 1)); //IAC pos
    list.append(parseByte(message, 39, 0, 1)); //Filtered load
    list.append(parseByte(message, 40, 0, 1)); //purge
    list.append(parseByte(message, 41, 0, 1)); //IAC no AC
    list.append(parseByte(message, 42, 0, 1)); //IAC with AC

    list.append(parseByte(message, 43, 0, 0.1)); //battery voltage
    list.append(parseWord(message, 44, 0, 0.35)); //spark advance
    list.append(parseWord(message, 46, 0, 0.015259));//injection pulse

    list.append(parseAFR(message, 48)); //AFR
    list.append(parseWord(message, 50, 0, 1));//running time

    list.append(parseAirFlow(message, 52));
    list.append(parseAirFlow(message, 54));

    list.append(parseByte(message, 56, 0, 0.02)); //CO potentiometer
    //list.append(parseSAPot());

    return list;
}

