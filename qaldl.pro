#-------------------------------------------------
#
# Project created by QtCreator 2014-01-30T19:56:19
#
#-------------------------------------------------

QT       += core gui
QT += serialport

greaterThan(QT_MAJOR_VERSION, 4): QT += widgets

TARGET = qaldl
TEMPLATE = app


SOURCES += main.cpp\
        mainwindow.cpp \
    CALDLReader.cpp \
    CISFI2SParser.cpp

HEADERS  += mainwindow.h \
    CALDLReader.h \
    IMessageParser.h \
    CISFI2SParser.h \
    SensorItem.h

FORMS    += mainwindow.ui
