/* Copyright (c) 2014, JohnJocke
 * BSD license, check file LICENSE
 */

#ifndef ALDLREADER_H
#define ALDLREADER_H

#include <QThread>
#include <QtSerialPort/QtSerialPort>

#include <IMessageParser.h>

class ALDLReader : public QThread
{
    Q_OBJECT
public:
    ALDLReader(QObject *parent = 0);
    ~ALDLReader();

    void setPort(const QString &port);
    void setECU(QString type);

    void close();

signals:
    void newValues(const QList<double> results);
    void status(const QString message);
    void sensorList(const QList<SensorItem> list);

private:
    virtual void run();

    bool createMessageParser();

    bool m_running;
    QString m_portName;
    QString m_ecutype;
    IMessageParser* m_parser;
};

#endif // ALDLREADER_H
