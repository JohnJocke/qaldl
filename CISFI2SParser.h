/* Copyright (c) 2014, JohnJocke
 * BSD license, check file LICENSE
 */

#ifndef CISFI2SPARSER_H
#define CISFI2SPARSER_H

#include <QByteArray>
#include "IMessageParser.h"

class CISFI2SParser : public IMessageParser
{
public:
    CISFI2SParser();
    virtual ~CISFI2SParser();

    virtual QList<SensorItem> getSensorList();
    virtual QByteArray getRequestMessage();
    virtual int getAnswerLength();
    virtual QList<double> parseMessage(const QByteArray& message);

private:
    double parseByte(const QByteArray &message, int idx, double offset, double mult);
    double parseRPM(const QByteArray &message, int idx);
    double parseWord(const QByteArray &message, int idx, double offset, double mult);
    double parseAirFlow(const QByteArray &message, int idx);
    double parseAFR(const QByteArray &message, int idx);

};

#endif // CISFI2SPARSER_H
