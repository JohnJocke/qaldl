/* Copyright (c) 2014, JohnJocke
 * BSD license, check file LICENSE
 */

#include "mainwindow.h"
#include "ui_mainwindow.h"

#include <QtSerialPort/QSerialPortInfo>
#include <QFileDialog>

MainWindow::MainWindow(QWidget *parent) :
    QMainWindow(parent),
    ui(new Ui::MainWindow)
{
    ui->setupUi(this);

    QList<QSerialPortInfo> list = QSerialPortInfo::availablePorts();
    QStringList names;
    foreach(QSerialPortInfo info, list) {
        names.append(info.portName());
    }
    names.sort();
    ui->comboBoxPort->addItems(names);

    connect(ui->pushButtonConnect, SIGNAL(clicked()), this, SLOT(btn_Connect()));
    connect(ui->pushButtonLogging, SIGNAL(clicked()), this, SLOT(btn_Logging()));

    m_reader = new ALDLReader(this);

    connect(m_reader, SIGNAL(status(QString)), this, SLOT(setStatus(QString)));
    connect(m_reader, SIGNAL(sensorList(QList<SensorItem>)), this, SLOT(setSensorList(QList<SensorItem>)));
    connect(m_reader, SIGNAL(newValues(QList<double>)), this, SLOT(updateValues(QList<double>)));

    statusBar()->showMessage("Choose serial port and connect.");

    m_file = new QFile(this);
    m_log = false;

    ui->comboBoxECU->addItem(QString("GM ISFI-2S"));
}

MainWindow::~MainWindow()
{
    m_reader->close();
    m_reader->wait();

    if(m_file->isOpen()) {
        m_file->close();
    }

    delete ui;
}

void MainWindow::setStatus(const QString message)
{
    statusBar()->showMessage(message);
}

void MainWindow::setSensorList(const QList<SensorItem> list)
{
    ui->tableWidget->setRowCount(list.size()/2);
    ui->tableWidget->setColumnCount(3);

    ui->tableWidget_2->setRowCount(list.size() - list.size()/2);
    ui->tableWidget_2->setColumnCount(3);

    int i = 0;
    for(i = 0; i < list.size()/2; ++i)
    {
        QTableWidgetItem *newItem = new QTableWidgetItem(list.at(i).name);
        ui->tableWidget->setItem(i, 0, newItem);

        newItem = new QTableWidgetItem(list.at(i).unit);
        ui->tableWidget->setItem(i, 2, newItem);

        //just to get some width for the value column
        newItem = new QTableWidgetItem("                                ");
        ui->tableWidget->setItem(i, 1, newItem);
    }

    for(int j = 0; j < list.size() - list.size()/2; ++j)
    {
        QTableWidgetItem *newItem = new QTableWidgetItem(list.at(i).name);
        ui->tableWidget_2->setItem(j, 0, newItem);

        newItem = new QTableWidgetItem(list.at(i).unit);
        ui->tableWidget_2->setItem(j, 2, newItem);

        //just to get some width for the value column
        newItem = new QTableWidgetItem("                                ");
        ui->tableWidget_2->setItem(j, 1, newItem);

        ++i;
    }

    ui->tableWidget->resizeColumnsToContents();
    ui->tableWidget_2->resizeColumnsToContents();

}

void MainWindow::btn_Connect()
{
    m_reader->close();
    m_reader->wait();

    m_reader->setPort(ui->comboBoxPort->currentText());
    m_reader->setECU(ui->comboBoxECU->currentText());
    m_reader->start();
}

void MainWindow::btn_Logging()
{
    if(m_file->isOpen()) {
        m_file->close();
        m_log = false;
        ui->pushButtonLogging->setText("Start logging");
        return;
    }

    ui->pushButtonLogging->setText("Stop logging");

    QString fileName = QFileDialog::getSaveFileName (this, "Open CSV file", QDir::currentPath(), "CSV (*.csv)");

    m_file->setFileName(fileName);
    m_file->open(QIODevice::ReadWrite | QIODevice::Append | QFile::Text);

    //write header to the file
    m_file->write("Timestamp");
    m_file->write(",");

    int i = 0;
    for(i = 0; i < ui->tableWidget->rowCount(); ++i) {
        //qDebug() << ui->tableWidget->item(i, 0)->text();
       m_file->write(ui->tableWidget->item(i, 0)->text().toStdString().c_str());
       m_file->write(",");
    }
    for(i = 0; i < ui->tableWidget_2->rowCount(); ++i) {
        //qDebug() << ui->tableWidget->item(i, 0)->text();
       m_file->write(ui->tableWidget_2->item(i, 0)->text().toStdString().c_str());
       m_file->write(",");
    }

    m_file->write("\n");

    m_log = true;
}

void MainWindow::updateValues(const QList<double> results)
{
    int i = 0;
    for(i = 0; i < results.size()/2; ++i)
    {
        QTableWidgetItem *newItem = new QTableWidgetItem(QString::number(results.at(i), 'f', 2));
        ui->tableWidget->setItem(i, 1, newItem);
    }

    for(int j = 0; j < results.size() - results.size()/2; ++j)
    {
        QTableWidgetItem *newItem = new QTableWidgetItem(QString::number(results.at(i), 'f', 2));
        ui->tableWidget_2->setItem(j, 1, newItem);

        ++i;
    }

    if(m_log) {
        saveValuestoCSV();
    }
}

void MainWindow::saveValuestoCSV()
{
    m_file->write(QDateTime::currentDateTime().toString(Qt::ISODate).toStdString().c_str());
    m_file->write(",");

    int i = 0;
    for(i = 0; i < ui->tableWidget->rowCount(); ++i) {
       m_file->write(ui->tableWidget->item(i, 1)->text().toStdString().c_str());
       m_file->write(",");
    }
    for(i = 0; i < ui->tableWidget_2->rowCount(); ++i) {
       m_file->write(ui->tableWidget_2->item(i, 1)->text().toStdString().c_str());
       m_file->write(",");
    }
    m_file->write("\n");
}
