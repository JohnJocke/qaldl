/* Copyright (c) 2014, JohnJocke
 * BSD license, check file LICENSE
 */

#include "CALDLReader.h"
#include "CISFI2SParser.h"

#include <QTime>

#define BLOCKING_TIMEOUT_MS 300

ALDLReader::ALDLReader(QObject *parent): QThread(parent), m_running(false)
{
    qRegisterMetaType< QList<SensorItem> >("QList<SensorItem>");
    qRegisterMetaType< QList<double> >("QList<double>");
}

ALDLReader::~ALDLReader()
{
}

void ALDLReader::setPort(const QString &port)
{
    m_portName = port;
}

void ALDLReader::setECU(QString type)
{
    m_ecutype = type;
}

void ALDLReader::close()
{
    m_running = false;
}

void ALDLReader::run()
{
    int ret = 0;

    if(m_portName.size() == 0) {
        return;
    }

    if(createMessageParser() == false) {
        return;
    }
    emit sensorList(m_parser->getSensorList());

    char answer[m_parser->getAnswerLength()];
    QSerialPort port;
    port.setPortName(m_portName);

    if(port.open(QIODevice::ReadWrite) == false) {
        emit status(QString("Could not open serial port!"));
        return;
    }
    emit status(QString("Serial port opened."));

    port.setBaudRate(8192);
    port.setDataBits(QSerialPort::Data8);
    port.setStopBits(QSerialPort::OneStop);
    port.setFlowControl(QSerialPort::NoFlowControl);
    port.setParity(QSerialPort::NoParity);

    QTime time;
    time.start();

    m_running = true;
    while(m_running)
    {
        qDebug() << "Elapsed: " << time.elapsed();
        time.restart();

        //send query
        port.write(m_parser->getRequestMessage());
        if(port.waitForBytesWritten(BLOCKING_TIMEOUT_MS) == false) {
            emit status(QString("Timeout when writing to serial port, retrying..."));
            continue;
        }

        //wait for message
        if(port.waitForReadyRead(BLOCKING_TIMEOUT_MS) == false) {
            emit status(QString("Serial port timeout. No connection to ECU."));
            continue;
        }
        ret = 0;
        while(m_running) {
            ret += port.read(&answer[ret], m_parser->getAnswerLength() - ret);
            if(ret >= m_parser->getAnswerLength()) {
                break;
            }
            if(port.waitForReadyRead(BLOCKING_TIMEOUT_MS) == false) {
                emit status(QString("Serial port timeout while waiting more bytes. No connection to ECU."));
                break;
            }
        }
        if(ret != m_parser->getAnswerLength()) {
            emit status(QString("Incomplete message, discarding..."));
            continue;
        }
        emit status(QString("Connection to ECU established!"));

        //parse message
        QList<double> results = m_parser->parseMessage(QByteArray(answer, m_parser->getAnswerLength()));

        //update GUI
        emit newValues(results);
    }

    port.close();
    emit status(QString("Serial port closed."));

    delete m_parser;
}

bool ALDLReader::createMessageParser()
{
    if(m_ecutype == "GM ISFI-2S") {
        m_parser = new CISFI2SParser();
    }
    else {
        emit status(QString("No valid ECU chosen!"));
        return false;
    }
    return true;
}
