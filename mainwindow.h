/* Copyright (c) 2014, JohnJocke
 * BSD license, check file LICENSE
 */

#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include <QMainWindow>

#include "CALDLReader.h"

namespace Ui {
class MainWindow;
}

class MainWindow : public QMainWindow
{
    Q_OBJECT
    
public:
    explicit MainWindow(QWidget *parent = 0);
   ~MainWindow();

public slots:
    void btn_Connect();
    void btn_Logging();
    void updateValues(const QList<double> results);
    void setStatus(const QString message);
    void setSensorList(const QList<SensorItem> list);
    
private:
    void saveValuestoCSV();

    Ui::MainWindow *ui;
    ALDLReader* m_reader;
    QFile* m_file;
    bool m_log;
};

#endif // MAINWINDOW_H
