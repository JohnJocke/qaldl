/* Copyright (c) 2014, JohnJocke
 * BSD license, check file LICENSE
 */

#ifndef SENSORITEM_H
#define SENSORITEM_H

#include <QString>

struct SensorItem
{
    SensorItem(QString _name, QString _unit) { name = _name; unit = _unit; }
    QString name;
    QString unit;
};

#endif // SENSORITEM_H
